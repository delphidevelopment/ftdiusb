unit FTDIusb;

interface

uses
  SysUtils, Classes, Windows, FTD2XX, StdCtrls, ExtCtrls, Messages, Controls;

const
  WM_USB_RX = WM_USER + 65;
  WM_USB_TX_EMPTY = WM_USER + 66;
  WM_USB_ERROR = WM_USER + 67;
  WM_OVERRUN_ERROR = WM_USER + 68;
  WM_PARITY_ERROR = WM_USER + 69;
  WM_FRAMING_ERROR = WM_USER + 70;
  WM_BREAK_INTERRUPT = WM_USER + 71;
  WM_WRITING_ERROR = WM_USER + 72;

type
  TRxEvent = procedure (Number: LongInt) of object;
  TTxEmptyEvent = procedure of object;
  TUsbErrorEvent = procedure (Error: string) of object;
  TReceiveDataEvent = procedure (Data: TBytes) of object;
  TReceiveStringEvent = procedure (Data: string) of object;

  TBaudRate = (baud300, baud600, baud1200, baud2400, baud4800, baud9600, baud14400, baud19200,
               baud38400, baud57600, baud115200, baud230400, baud460800, baud921600);
  TDataBits = (databits7, databits8);
  TStopBits = (stopbits1, stopbits2);
  TParity = (parityNone, parityOdd, parityEven, parityMark, paritySpace);
  TFlowControl = (flowNone, flowRTS_CTS, flowDTR_DSR, flowXON_XOFF);
  TSelectionType = (stBaudRate, stDataBits, stStopBits, stParity, stFlowControl);

  TStatusThread = class(TThread)
  private
    FUsbHWnd: HWND;
    FUsbHandle: DWORD;
    FTxEmpty: Boolean;
    FWaitInterval: DWORD;
  protected
    procedure Execute; override;
  public
    constructor Create(AUsbHWnd: HWND; AUsbHandle: DWORD);
    procedure ClearTxEmpty;
    property WaitInterval: DWORD read FWaitInterval write FWaitInterval;
  end;

  TWritingThread = class(TThread)
  private
    FUsbHWnd: HWND;
    FUsbHandle: DWORD;
    FBuffer: TBytes;
  protected
    procedure Execute; override;
  public
    constructor Create(AUsbHWnd: HWND; AUsbHandle: DWORD);
    procedure StartWriting(const AData: TBytes);
    procedure StartWritingStr(const AData: string);
  end;

  TFtdiLabeledComboBox = class;

  TCustomFtdiUsb = class(TComponent)
  private
    { Private declarations }
    FUsbHWnd: HWND;
    FLocationId: DWORD;
    FUsbHandle: DWORD;
    FUsbOpen: Boolean;
    FStatusThread: TStatusThread;
    FWritingThread: TWritingThread;
    FReceiveEnabled: Boolean;
    FFtdiLabeledComboBox: TFtdiLabeledComboBox;
    FBaudRate: DWORD;
    FDataBits: Byte;
    FStopBits: Byte;
    FParity: Byte;
    FFlowControl: Word;

    FOnReceiveData: TReceiveDataEvent;
    FOnReceiveString: TReceiveStringEvent;
    FOnSendQueueEmpty: TTxEmptyEvent;
    FOnUsbError: TUsbErrorEvent;
    FOnCommError: TUsbErrorEvent;
    class var
      UsbDeviceCount: DWORD;
      UsbDeviceInfoList: array of FT_Device_Info_Node;

    class function GetUsbDeviceInfoList: FT_Result;
    procedure FtdiUsbMessage(var UsbMessage: TMessage);
    procedure StatusThreadTerminated(Sender: TObject);
    procedure WritingThreadTerminated(Sender: TObject);
  protected
    { Protected declarations }
    procedure SetFtdiLocationId(Value: DWORD);
    procedure SetFtdiLabeledComboBox(const Value: TFtdiLabeledComboBox);
    function GetFtdiBaudRate: TBaudRate;
    procedure SetFtdiBaudRate(Value: TBaudRate);
    function GetFtdiDataBits: TDataBits;
    procedure SetFtdiDataBits(Value: TDataBits);
    function GetFtdiStopBits: TStopBits;
    procedure SetFtdiStopBits(Value: TStopBits);
    function GetFtdiParity: TParity;
    procedure SetFtdiParity(Value: TParity);
    function GetFtdiFlowControl: TFlowControl;
    procedure SetFtdiFlowControl(Value: TFlowControl);
    property BaudRate: TBaudRate read GetFtdiBaudRate write SetFtdiBaudRate default baud1200;
    property DataBits: TDataBits read GetFtdiDataBits write SetFtdiDataBits default databits8;
    property StopBits: TStopBits read GetFtdiStopBits write SetFtdiStopBits default stopbits1;
    property Parity: TParity read GetFtdiParity write SetFtdiParity default parityOdd;
    property FlowControl: TFlowControl read GetFtdiFlowControl write SetFtdiFlowControl default flowNone;
    property ReceiveEnabled: Boolean read FReceiveEnabled write FReceiveEnabled default False;
    property OnReceiveData: TReceiveDataEvent read FOnReceiveData write FOnReceiveData;
    property OnReceiveString: TReceiveStringEvent read FOnReceiveString write FOnReceiveString;
    property OnSendQueueEmpty: TTxEmptyEvent read FOnSendQueueEmpty write FOnSendQueueEmpty;
    property OnUsbError: TUsbErrorEvent read FOnUsbError write FOnUsbError;
    property OnCommError: TUsbErrorEvent read FOnCommError write FOnCommError;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    class function GetUsbDeviceList: TStringList;
    class function GetLibraryVersion: string;
    function SetLocationId(LocId: DWORD): Boolean; overload;
    function SetLocationId(UsbDescription: string): Boolean; overload;
    function SetLocationId(DevType: DWORD; UsbDescription: string): Boolean; overload;
    function OpenUsb: Boolean;
    procedure CloseUsb;
    function Write(const Data: TBytes): Boolean; overload;
    function Write(const Data: string): Boolean; overload;
    function GetDriverVersion: string;
    function SetBaudRate(ABaudRate: TBaudRate): Boolean;
    function SetDataCharacteristics(ADataBits: TDataBits; AStopBits: TStopBits; AParity: TParity): Boolean;
    function SetTimeouts(AReadTimeout, AWriteTimeout: DWORD): Boolean;
    function SetFlowControl(AFlowControl: TFlowControl; AXon, AXoff: Byte): Boolean;
    function SetDTR: Boolean;
    function ClearDTR: Boolean;
    function SetRTS: Boolean;
    function ClearRTS: Boolean;
    function ResetDevice: Boolean;
    function ResetPort: Boolean;
    function CyclePort: Boolean;
    property LocationId: DWORD read FLocationId;
    property Baud: DWORD read FBaudRate;
  end;

  TFtdiUsb = class(TCustomFtdiUsb)
  published
    { Published declarations }
    property BaudRate;
    property DataBits;
    property StopBits;
    property Parity;
    property FlowControl;
    property ReceiveEnabled;
    property OnReceiveData;
    property OnReceiveString;
    property OnSendQueueEmpty;
    property OnUsbError;
    property OnCommError;
  end;

  TALabel = class(TCustomLabel)
  private
    function GetTop: Integer;
    function GetLeft: Integer;
    function GetWidth: Integer;
    function GetHeight: Integer;
    procedure SetHeight(const Value: Integer);
    procedure SetWidth(const Value: Integer);
  protected
    procedure AdjustBounds; override;
  public
    constructor Create(AOwner: TComponent); override;
  published
    property Caption;
    property Font;
    property Height: Integer read GetHeight write SetHeight;
    property Left: Integer read GetLeft;
    property Top: Integer read GetTop;
    property Width: Integer read GetWidth write SetWidth;
  end;

  TCustomALabeledComboBox = class(TCustomComboBox)
  private
    { Private declarations }
    FComboBoxLabel: TALabel;
    FLabelPosition: TLabelPosition;
    FLabelSpacing: Integer;
    procedure SetLabelPosition(const Value: TLabelPosition);
    procedure SetLabelSpacing(const Value: Integer);
  protected
    { Protected declarations }
    procedure SetParent(AParent: TWinControl); override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetName(const Value: TComponentName); override;
    procedure CMVisiblechanged(var Message: TMessage); message CM_VISIBLECHANGED;
    procedure CMEnabledchanged(var Message: TMessage); message CM_ENABLEDCHANGED;
    procedure CMBidimodechanged(var Message: TMessage); message CM_BIDIMODECHANGED;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
    procedure SetBounds(ALeft: Integer; ATop: Integer; AWidth: Integer; AHeight: Integer); override;
    property ComboBoxLabel: TALabel read FComboBoxLabel;
    property LabelPosition: TLabelPosition read FLabelPosition write SetLabelPosition default lpAbove;
    property LabelSpacing: Integer read FLabelSpacing write SetLabelSpacing default 5;
  end;

  TALabeledComboBox = class(TCustomALabeledComboBox)
  published
    { Published declarations }
    property Style default csDropDownList; {Must be published before Items}
    property Color;
    property ComboBoxLabel;
    property DropDownCount;
    property Enabled;
    property Font;
    property LabelPosition;
    property LabelSpacing;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnChange;
    property OnClick;
    property OnEnter;
    property OnExit;
    property Items; { Must be published after OnMeasureItem }
  end;

  TSerialLabeledComboBox = class(TCustomALabeledComboBox)
  private
    { Private declarations }
    FSelectionType: TSelectionType;
    FTransmitter: TFtdiUsb;
    FReceiver: TFtdiUsb;
    procedure SetSelectionType(const Value: TSelectionType);
    procedure FilloutItems;
    procedure SetTransmitter(const Value: TFtdiUsb);
    procedure SetReceiver(const Value: TFtdiUsb);
    procedure TransmitterSetting;
    procedure ReceiverSetting;
  protected
    { Protected declarations }
    procedure Change; override;
  public
    { Public declarations }
    constructor Create(AOwner: TComponent); override;
  published
    { Published declarations }
    property Color;
    property ComboBoxLabel;
    property DropDownCount;
    property Enabled;
    property Font;
    property LabelPosition;
    property LabelSpacing;
    property SelectionType: TSelectionType read FSelectionType write SetSelectionType default stBaudRate;
    property Transmitter: TFtdiUsb read FTransmitter write SetTransmitter;
    property Receiver: TFtdiUsb read FReceiver write SetReceiver;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnChange;
    property OnClick;
    property OnEnter;
    property OnExit;
  end;

  TFtdiLabeledComboBox = class(TCustomALabeledComboBox)
  private
    { Private declarations }
    FFtdiUsb: TFtdiUsb;
    FAutoUpdate: Boolean;
    procedure SetFtdiUsb(const Value: TFtdiUsb);
  protected
    { Protected declarations }
    procedure Change; override;
    procedure DoEnter; override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer); override;
  public
    { Public declarations }
    procedure UpdateUsbDevices;
  published
    { Published declarations }
    property Color;
    property ComboBoxLabel;
    property DropDownCount;
    property Enabled;
    property Font;
    property LabelPosition;
    property LabelSpacing;
    property FtdiUsb: TFtdiUsb read FFtdiUsb write SetFtdiUsb;
    property AutoUpdate: Boolean read FAutoUpdate write FAutoUpdate;
    property TabOrder;
    property TabStop;
    property Visible;
    property OnChange;
    property OnClick;
    property OnEnter;
    property OnExit;
  end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('FTDI Comp', [TFtdiUsb, TALabeledComboBox, TSerialLabeledComboBox, TFtdiLabeledComboBox]);
end;

{ TCustomFtdiUsb }

constructor TCustomFtdiUsb.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FUsbHWnd:= AllocateHWnd(FtdiUsbMessage);
  FLocationId:= 0;
  FUsbOpen:= False;
  FReceiveEnabled:= False;

  FBaudRate:= FT_BAUD_1200;
  FDataBits:= FT_DATA_BITS_8;
  FStopBits:= FT_STOP_BITS_1;
  FParity:= FT_PARITY_ODD;
  FFlowControl:= FT_FLOW_NONE;
end;

destructor TCustomFtdiUsb.Destroy;
begin
  DeallocateHWnd(FUsbHWnd);

  inherited Destroy;
end;

class function TCustomFtdiUsb.GetUsbDeviceInfoList: FT_Result;
begin
  Result:= FT_CreateDeviceInfoList(@UsbDeviceCount);
  if Result=FT_OK then
  begin
    SetLength(UsbDeviceInfoList, UsbDeviceCount);
    Result:= FT_GetDeviceInfoList(UsbDeviceInfoList, @UsbDeviceCount);
  end;
end;

class function TCustomFtdiUsb.GetUsbDeviceList: TStringList;
var
  UsbIndex: DWORD;
begin
  Result:= TStringList.Create;

  if GetUsbDeviceInfoList=FT_OK then
  begin
    if UsbDeviceCount=0 then
      Exit;

    for UsbIndex:= 0 to UsbDeviceCount - 1 do
    begin
      Result.Append(IntToStr(UsbDeviceInfoList[UsbIndex].LocID)+' - '+string(AnsiString(UsbDeviceInfoList[UsbIndex].Description)));
    end;
  end;
end;

class function TCustomFtdiUsb.GetLibraryVersion: string;
var
  DLLVersion, MajorVer, MinorVer, BuildVer: DWORD;
begin
  if FT_GetLibraryVersion(@DLLVersion)=FT_OK then
  begin
    BuildVer:= DLLVersion and $FF;
    MinorVer:= (DLLVersion shr 8) and $FF;
    MajorVer:= (DLLVersion shr 16) and $FF;
    Result:= IntToHex(MajorVer, 1) + '.' + IntToHex(MinorVer, 2) + '.' + IntToHex(BuildVer, 2);
  end
  else
    Result:= 'Not Available';
end;

procedure TCustomFtdiUsb.FtdiUsbMessage(var UsbMessage: TMessage);
var
  RxBuffer: TBytes;
  RxReturned: DWORD;
begin
  case UsbMessage.Msg of
    WM_USB_RX: begin
          SetLength(RxBuffer, UsbMessage.WParam);
          if FT_Read(FUsbHandle, RxBuffer, UsbMessage.WParam, @RxReturned)=FT_OK then
          begin
            if (RxReturned>0) and FReceiveEnabled and Assigned(FOnReceiveData) then
              FOnReceiveData(RxBuffer);
          end;
    end;
    WM_USB_TX_EMPTY: if Assigned(FOnSendQueueEmpty) then FOnSendQueueEmpty;
    WM_USB_ERROR: if Assigned(FOnUsbError) then FOnUsbError('USB device error '+IntToStr(UsbMessage.WParam)+'. (FT_Result: '+IntToStr(UsbMessage.LParam)+')');
    WM_OVERRUN_ERROR: if Assigned(FOnCommError) then FOnCommError('Overrun error. (Line status: 0x'+IntToHex(UsbMessage.LParam, 2)+')');
    WM_PARITY_ERROR: if Assigned(FOnCommError) then FOnCommError('Parity error. (Line status: 0x'+IntToHex(UsbMessage.LParam, 2)+')');
    WM_FRAMING_ERROR: if Assigned(FOnCommError) then FOnCommError('Framing error. (Line status: 0x'+IntToHex(UsbMessage.LParam, 2)+')');
    WM_BREAK_INTERRUPT: if Assigned(FOnCommError) then FOnCommError('Break interrupt. (Line status: 0x'+IntToHex(UsbMessage.LParam, 2)+')');
    WM_WRITING_ERROR: if Assigned(FOnCommError) then FOnCommError('Writing error. (FT_Result: '+IntToStr(UsbMessage.LParam)+')');
    else
      UsbMessage.Result:= DefWindowProc(FUsbHWnd, UsbMessage.Msg, UsbMessage.WParam, UsbMessage.LParam);
  end;
end;

procedure TCustomFtdiUsb.SetFtdiLocationId(Value: DWORD);
begin
  if Value<>FLocationId then
    FLocationId:= Value;
end;

procedure TCustomFtdiUsb.SetFtdiLabeledComboBox(const Value: TFtdiLabeledComboBox);
begin
  if Value<>FFtdiLabeledComboBox then
    FFtdiLabeledComboBox:= Value;
end;

function TCustomFtdiUsb.GetFtdiBaudRate: TBaudRate;
begin
  case FBaudRate of
    FT_BAUD_300: Result:= baud300;
    FT_BAUD_600: Result:= baud600;
    FT_BAUD_1200: Result:= baud1200;
    FT_BAUD_2400: Result:= baud2400;
    FT_BAUD_4800: Result:= baud4800;
    FT_BAUD_9600: Result:= baud9600;
    FT_BAUD_14400: Result:= baud14400;
    FT_BAUD_19200: Result:= baud19200;
    FT_BAUD_38400: Result:= baud38400;
    FT_BAUD_57600: Result:= baud57600;
    FT_BAUD_115200: Result:= baud115200;
    FT_BAUD_230400: Result:= baud230400;
    FT_BAUD_460800: Result:= baud460800;
    FT_BAUD_921600: Result:= baud921600;
    else Result:= baud1200;
  end;
end;

function TCustomFtdiUsb.GetFtdiDataBits: TDataBits;
begin
  if FDataBits=FT_DATA_BITS_7 then
    Result:= databits7
  else
    Result:= databits8;
end;

function TCustomFtdiUsb.GetFtdiFlowControl: TFlowControl;
begin
  case FFlowControl of
    FT_FLOW_NONE: Result:= flowNone;
    FT_FLOW_RTS_CTS: Result:= flowRTS_CTS;
    FT_FLOW_DTR_DSR: Result:= flowDTR_DSR;
    FT_FLOW_XON_XOFF: Result:= flowXON_XOFF;
    else Result:= flowNone;
  end;
end;

function TCustomFtdiUsb.GetFtdiParity: TParity;
begin
  Result:= TParity(FParity);
end;

function TCustomFtdiUsb.GetFtdiStopBits: TStopBits;
begin
  if FStopBits=FT_STOP_BITS_2 then
    Result:= stopbits2
  else
    Result:= stopbits1;
end;

procedure TCustomFtdiUsb.SetFtdiBaudRate(Value: TBaudRate);
begin
  case Value of
    baud300: FBaudRate:= FT_BAUD_300;
    baud600: FBaudRate:= FT_BAUD_600;
    baud1200: FBaudRate:= FT_BAUD_1200;
    baud2400: FBaudRate:= FT_BAUD_2400;
    baud4800: FBaudRate:= FT_BAUD_4800;
    baud9600: FBaudRate:= FT_BAUD_9600;
    baud14400: FBaudRate:= FT_BAUD_14400;
    baud19200: FBaudRate:= FT_BAUD_19200;
    baud38400: FBaudRate:= FT_BAUD_38400;
    baud57600: FBaudRate:= FT_BAUD_57600;
    baud115200: FBaudRate:= FT_BAUD_115200;
    baud230400: FBaudRate:= FT_BAUD_230400;
    baud460800: FBaudRate:= FT_BAUD_460800;
    baud921600: FBaudRate:= FT_BAUD_921600;
  end;
end;

procedure TCustomFtdiUsb.SetFtdiDataBits(Value: TDataBits);
begin
  case Value of
    databits7: FDataBits:= FT_DATA_BITS_7;
    databits8: FDataBits:= FT_DATA_BITS_8;
  end;
end;

procedure TCustomFtdiUsb.SetFtdiFlowControl(Value: TFlowControl);
begin
  case Value of
    flowNone: FFlowControl:= FT_FLOW_NONE;
    flowRTS_CTS: FFlowControl:= FT_FLOW_RTS_CTS;
    flowDTR_DSR: FFlowControl:= FT_FLOW_DTR_DSR;
    flowXON_XOFF: FFlowControl:= FT_FLOW_XON_XOFF;
  end;
end;

procedure TCustomFtdiUsb.SetFtdiParity(Value: TParity);
begin
  FParity:= Integer(Value);
{  case Value of
    parityNone: FParity:= FT_PARITY_NONE;
    parityOdd: ;
    parityEven: ;
    parityMark: ;
    paritySpace: ;
  end;}
end;

procedure TCustomFtdiUsb.SetFtdiStopBits(Value: TStopBits);
begin
  case Value of
    stopbits1: FStopBits:= FT_STOP_BITS_1;
    stopbits2: FStopBits:= FT_STOP_BITS_2;
  end;
end;

function TCustomFtdiUsb.SetLocationId(LocId: DWORD): Boolean;
var
  UsbIndex: DWORD;
begin
  Result:= False;
  if FUsbOpen then
    Exit;

  if GetUsbDeviceInfoList=FT_OK then
  begin
    if UsbDeviceCount=0 then
      Exit;

    for UsbIndex:= 0 to UsbDeviceCount - 1 do
    begin
      if UsbDeviceInfoList[UsbIndex].LocID=LocId then
      begin
        FLocationId:= LocId;
        Result:= True;
      end;
    end;
  end;
end;

function TCustomFtdiUsb.SetLocationId(UsbDescription: string): Boolean;
var
  UsbIndex: DWORD;
begin
  Result:= False;
  if FUsbOpen then
    Exit;

  if GetUsbDeviceInfoList=FT_OK then
  begin
    if UsbDeviceCount=0 then
      Exit;

    for UsbIndex:= 0 to UsbDeviceCount - 1 do
    begin
      if string(AnsiString(UsbDeviceInfoList[UsbIndex].Description))=UsbDescription then
      begin
        FLocationId:= UsbDeviceInfoList[UsbIndex].LocID;
        Result:= True;
      end;
    end;
  end;
end;

function TCustomFtdiUsb.SetLocationId(DevType: DWORD; UsbDescription: string): Boolean;
var
  UsbIndex: DWORD;
begin
  Result:= False;
  if FUsbOpen then
    Exit;

  if GetUsbDeviceInfoList=FT_OK then
  begin
    if UsbDeviceCount=0 then
      Exit;

    for UsbIndex:= 0 to UsbDeviceCount - 1 do
    begin
      if UsbDeviceInfoList[UsbIndex].DeviceType=DevType then
      begin
        if string(AnsiString(UsbDeviceInfoList[UsbIndex].Description))=UsbDescription then
        begin
          FLocationId:= UsbDeviceInfoList[UsbIndex].LocID;
          Result:= True;
        end;
      end;
    end;
  end;
end;

procedure TCustomFtdiUsb.StatusThreadTerminated(Sender: TObject);
begin
  FT_Close(FUsbHandle);
  FUsbOpen:= False;
  FStatusThread:= nil;
  if FFtdiLabeledComboBox<>nil then
    FFtdiLabeledComboBox.Enabled:= True;
end;

procedure TCustomFtdiUsb.WritingThreadTerminated(Sender: TObject);
begin
  FWritingThread:= nil;
end;

function TCustomFtdiUsb.OpenUsb: Boolean;
begin
  FUsbOpen:= False;
  Result:= False;
  if (FLocationId=0) or (FUsbHWnd=0) then
    Exit;

  if FT_OpenByLocation(FLocationId, FT_OPEN_BY_LOCATION, @FUsbHandle)<>FT_OK then
    Exit;

  if FT_SetBaudRate(FUsbHandle, FBaudRate)<>FT_OK then
  begin
    FT_Close(FUsbHandle);
    Exit;
  end;
  if FT_SetDataCharacteristics(FUsbHandle, FDataBits, FStopBits, FParity)<>FT_OK then
  begin
    FT_Close(FUsbHandle);
    Exit;
  end;
  if FT_SetFlowControl(FUsbHandle, FFlowControl, 0, 0)<>FT_OK then
  begin
    FT_Close(FUsbHandle);
    Exit;
  end;
  FUsbOpen:= True;
  if FFtdiLabeledComboBox<>nil then
  begin
    FFtdiLabeledComboBox.Enabled:= False;
    FFtdiLabeledComboBox.CustomHint.Description := 'DLL version: ' + GetLibraryVersion + #13 + #10 + 'Driver version: ' + GetDriverVersion + #13 + #10;
  end;

  FStatusThread:= TStatusThread.Create(FUsbHWnd, FUsbHandle);
  FStatusThread.OnTerminate:= StatusThreadTerminated;
  FStatusThread.Start;
  Result:= True;
end;

procedure TCustomFtdiUsb.CloseUsb;
begin
  if FStatusThread<>nil then
  begin
    if not FStatusThread.Terminated then
      FStatusThread.Terminate;

    FStatusThread:= nil;
  end
  else
  begin
    if FUsbOpen then
    begin
      FT_Close(FUsbHandle);
      FUsbOpen:= False;
      if FFtdiLabeledComboBox<>nil then
        FFtdiLabeledComboBox.Enabled:= True;
    end;
  end;
end;

function TCustomFtdiUsb.Write(const Data: TBytes): Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FWritingThread=nil then
  begin
    FWritingThread:= TWritingThread.Create(FUsbHWnd, FUsbHandle);
    FWritingThread.OnTerminate:= WritingThreadTerminated;
    FWritingThread.StartWriting(Data);
    FStatusThread.ClearTxEmpty;
    Result:= True;
  end;
end;

function TCustomFtdiUsb.Write(const Data: string): Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FWritingThread=nil then
  begin
    FWritingThread:= TWritingThread.Create(FUsbHWnd, FUsbHandle);
    FWritingThread.OnTerminate:= WritingThreadTerminated;
    FWritingThread.StartWritingStr(Data);
    FStatusThread.ClearTxEmpty;
    Result:= True;
  end;
end;

function TCustomFtdiUsb.GetDriverVersion: string;
var
  DriverVer, MajorVer, MinorVer, BuildVer: DWORD;
begin
  Result:= 'Not Available';
  if not FUsbOpen then
    Exit;

  if FT_GetDriverVersion(FUsbHandle, @DriverVer)=FT_OK then
  begin
    BuildVer:= DriverVer and $FF;
    MinorVer:= (DriverVer shr 8) and $FF;
    MajorVer:= (DriverVer shr 16) and $FF;
    Result:= IntToHex(MajorVer, 1) + '.' + IntToHex(MinorVer, 2) + '.' + IntToHex(BuildVer, 2);
  end;
end;

function TCustomFtdiUsb.SetBaudRate(ABaudRate: TBaudRate): Boolean;
var
  Temp: DWORD;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  Temp:= FBaudRate;
  SetFtdiBaudRate(ABaudRate);
  if FT_SetBaudRate(FUsbHandle, FBaudRate)=FT_OK then
    Result:= True
  else
    FBaudRate:= Temp;
end;

function TCustomFtdiUsb.SetDataCharacteristics(ADataBits: TDataBits; AStopBits: TStopBits; AParity: TParity): Boolean;
var
  TempD, TempS, TempP: Byte;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  TempD:= FDataBits;
  SetFtdiDataBits(ADataBits);
  TempS:= FStopBits;
  SetFtdiStopBits(AStopBits);
  TempP:= FParity;
  SetFtdiParity(AParity);
  if FT_SetDataCharacteristics(FUsbHandle, FDataBits, FStopBits, FParity)=FT_OK then
    Result:= True
  else
  begin
    FDataBits:= TempD;
    FStopBits:= TempS;
    FParity:= TempP;
  end;
end;

function TCustomFtdiUsb.SetTimeouts(AReadTimeout, AWriteTimeout: DWORD): Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FT_SetTimeouts(FUsbHandle, AReadTimeout, AWriteTimeout)=FT_OK then
    Result:= True;
end;

function TCustomFtdiUsb.SetFlowControl(AFlowControl: TFlowControl; AXon, AXoff: UCHAR): Boolean;
var
  Temp: Word;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  Temp:= FFlowControl;
  SetFtdiFlowControl(AFlowControl);
  if FT_SetFlowControl(FUsbHandle, FFlowControl, AXon, AXoff)=FT_OK then
    Result:= True
  else
    FFlowControl:= Temp;
end;

function TCustomFtdiUsb.SetDTR: Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FT_SetDtr(FUsbHandle)=FT_OK then
    Result:= True;
end;

function TCustomFtdiUsb.ClearDTR: Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FT_ClrDtr(FUsbHandle)=FT_OK then
    Result:= True;
end;

function TCustomFtdiUsb.SetRTS: Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FT_SetRts(FUsbHandle)=FT_OK then
    Result:= True;
end;

function TCustomFtdiUsb.ClearRTS: Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FT_ClrRts(FUsbHandle)=FT_OK then
    Result:= True;
end;

function TCustomFtdiUsb.ResetDevice: Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FT_ResetDevice(FUsbHandle)=FT_OK then
    Result:= True;
end;

function TCustomFtdiUsb.ResetPort: Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FT_ResetPort(FUsbHandle)=FT_OK then
    Result:= True;
end;

function TCustomFtdiUsb.CyclePort: Boolean;
begin
  Result:= False;
  if not FUsbOpen then
    Exit;

  if FT_CyclePort(FUsbHandle)=FT_OK then
    Result:= True;
end;

{ TFtdiUsb }

{ TStatusThread }

constructor TStatusThread.Create(AUsbHWnd: HWND; AUsbHandle: DWORD);
begin
  inherited Create(True);

  FUsbHWnd:= AUsbHWnd;
  FUsbHandle:= AUsbHandle;
  FTxEmpty:= True;
  FWaitInterval:= 100;
  FreeOnTerminate:= True;
end;

procedure TStatusThread.Execute;
var
  EventHandle: THandle;
  EventMask, Rx, Tx, Status, LineStatus: DWORD;
  Res: FT_Result;
begin
  EventHandle:= CreateEvent(nil, False, False, '');
  EventMask:= FT_EVENT_RXCHAR or FT_EVENT_LINE_STATUS;

  Res:= FT_SetEventNotification(FUsbHandle, EventMask, EventHandle);
  if Res=FT_OK then
  begin
    while not Terminated do
    begin
      LineStatus:= 0;
      WaitForSingleObject(EventHandle, FWaitInterval);
      Res:= FT_GetStatus(FUsbHandle, @Rx, @Tx, @Status);
      if Res=FT_OK then
      begin
        if (Status and FT_EVENT_LINE_STATUS)<>0 then
        begin
          if FT_GetModemStatus(FUsbHandle, @LineStatus)=FT_OK then
          begin
            LineStatus:= (LineStatus shr 8)  and $FF;
            if (LineStatus and OE)<>0 then
              PostMessage(FUsbHWnd, WM_OVERRUN_ERROR, Rx, LineStatus);
            if (LineStatus and PE)<>0 then
              PostMessage(FUsbHWnd, WM_PARITY_ERROR, Rx, LineStatus);
            if (LineStatus and FE)<>0 then
              PostMessage(FUsbHWnd, WM_FRAMING_ERROR, Rx, LineStatus);
            if (LineStatus and BI)<>0 then
              PostMessage(FUsbHWnd, WM_BREAK_INTERRUPT, Rx, LineStatus);
          end;
        end;
        if (Rx>0) then
          SendMessage(FUsbHWnd, WM_USB_RX, Rx, LineStatus);

        if (Tx=0) and not FTxEmpty then
        begin
          FTxEmpty:= True;
          SendMessage(FUsbHWnd, WM_USB_TX_EMPTY, 0, 0);
        end;
      end
      else
      begin
        Terminate;
        PostMessage(FUsbHWnd, WM_USB_ERROR, 2, Res);
      end;
    end;
  end
  else
  begin
    Terminate;
    PostMessage(FUsbHWnd, WM_USB_ERROR, 1, Res);
  end;
  CloseHandle(EventHandle);
end;

procedure TStatusThread.ClearTxEmpty;
begin
  FTxEmpty:= False;
end;

{ TWritingThread }

constructor TWritingThread.Create(AUsbHWnd: HWND; AUsbHandle: DWORD);
begin
  inherited Create(True);

  FUsbHWnd:= AUsbHWnd;
  FUsbHandle:= AUsbHandle;
  FreeOnTerminate:= True;
end;

procedure TWritingThread.Execute;
var
  TxWritten: DWORD;
  Res: FT_Result;
begin
  Res:= FT_Write(FUsbHandle, FBuffer, Length(FBuffer), @TxWritten);
  if Res<>FT_OK then
    PostMessage(FUsbHWnd, WM_WRITING_ERROR, TxWritten, Res);
end;

procedure TWritingThread.StartWriting(const AData: TBytes);
var
  I, BufSize: Integer;
begin
  BufSize:= Length(AData);
  SetLength(FBuffer, BufSize);
  for I:= 0 to BufSize - 1 do
    FBuffer[I]:= AData[I];

  Start;
end;

procedure TWritingThread.StartWritingStr(const AData: string);
begin
  FBuffer:= BytesOf(AData);

  Start;
end;

{ TALabel }

constructor TALabel.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  SetSubComponent(True);
end;

procedure TALabel.AdjustBounds;
begin
  inherited AdjustBounds;

  if Owner is TFtdiLabeledComboBox then
    with Owner as TFtdiLabeledComboBox do
      SetBounds(Left, Top, Width, Height);
end;

function TALabel.GetHeight: Integer;
begin
  Result := inherited Height;
end;

function TALabel.GetLeft: Integer;
begin
  Result := inherited Left;
end;

function TALabel.GetTop: Integer;
begin
  Result := inherited Top;
end;

function TALabel.GetWidth: Integer;
begin
  Result := inherited Width;
end;

procedure TALabel.SetHeight(const Value: Integer);
begin
  SetBounds(Left, Top, Width, Value);
end;

procedure TALabel.SetWidth(const Value: Integer);
begin
  SetBounds(Left, Top, Value, Height);
end;

{ TCustomALabeledComboBox }

constructor TCustomALabeledComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Style:= csDropDownList;
  Width:= 100;
  FLabelPosition:= lpAbove;
  FLabelSpacing:= 5;
  if FComboBoxLabel=nil then
  begin
    FComboBoxLabel:= TALabel.Create(Self);
    FComboBoxLabel.Name:= 'NameLabel';
    FComboBoxLabel.Caption:= 'Denomination';
    FComboBoxLabel.FreeNotification(Self);
    FComboBoxLabel.FocusControl:= Self;
  end;
end;

procedure TCustomALabeledComboBox.CMBidimodechanged(var Message: TMessage);
begin
  inherited;
  if FComboBoxLabel<>nil then
    FComboBoxLabel.BiDiMode:= BiDiMode;
end;

procedure TCustomALabeledComboBox.CMEnabledchanged(var Message: TMessage);
begin
  inherited;
  if FComboBoxLabel<>nil then
    FComboBoxLabel.Enabled:= Enabled;
end;

procedure TCustomALabeledComboBox.CMVisiblechanged(var Message: TMessage);
begin
  inherited;
  if FComboBoxLabel<>nil then
    FComboBoxLabel.Visible:= Visible;
end;

procedure TCustomALabeledComboBox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);

  if (AComponent=FComboBoxLabel) and (Operation=opRemove) then
    FComboBoxLabel:= nil;
end;

procedure TCustomALabeledComboBox.SetBounds(ALeft, ATop, AWidth, AHeight: Integer);
begin
  inherited SetBounds(ALeft, ATop, AWidth, AHeight);

  SetLabelPosition(FLabelPosition);
end;

procedure TCustomALabeledComboBox.SetLabelPosition(const Value: TLabelPosition);
var
  P: TPoint;
begin
  if FComboBoxLabel=nil then
    Exit;

  FLabelPosition:= Value;
  case Value of
    lpAbove: P := Point(Left, Top - FComboBoxLabel.Height - FLabelSpacing);
    lpBelow: P := Point(Left, Top + Height + FLabelSpacing);
    lpLeft : P := Point(Left - FComboBoxLabel.Width - FLabelSpacing, Top + ((Height - FComboBoxLabel.Height) div 2));
    lpRight: P := Point(Left + Width + FLabelSpacing, Top + ((Height - FComboBoxLabel.Height) div 2));
  end;
  FComboBoxLabel.SetBounds(P.x, P.y, FComboBoxLabel.Width, FComboBoxLabel.Height);
end;

procedure TCustomALabeledComboBox.SetLabelSpacing(const Value: Integer);
begin
  FLabelSpacing := Value;
  SetLabelPosition(FLabelPosition);
end;

procedure TCustomALabeledComboBox.SetName(const Value: TComponentName);
var
  LClearText: Boolean;
begin
  LClearText:= (csDesigning in ComponentState) and (Text='');

  inherited SetName(Value);

  if LClearText then
    Text:= '';
end;

procedure TCustomALabeledComboBox.SetParent(AParent: TWinControl);
begin
  inherited SetParent(AParent);

  if FComboBoxLabel<>nil then
    FComboBoxLabel.Parent:= AParent;
end;

{ TSerialLabeledComboBox }

constructor TSerialLabeledComboBox.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FSelectionType:= stBaudRate;
  if not (csDesigning in ComponentState) then
    FilloutItems;
end;

procedure TSerialLabeledComboBox.Change;
begin
  if FTransmitter<>nil then
  begin
    case FSelectionType of
      stBaudRate: FTransmitter.BaudRate:= TBaudRate(ItemIndex);
      stDataBits: FTransmitter.DataBits:= TDataBits(ItemIndex);
      stStopBits: FTransmitter.StopBits:= TStopBits(ItemIndex);
      stParity: FTransmitter.Parity:= TParity(ItemIndex);
      stFlowControl: FTransmitter.FlowControl:= TFlowControl(ItemIndex);
    end;
  end;
  if FReceiver<>nil then
  begin
    case FSelectionType of
      stBaudRate: FReceiver.BaudRate:= TBaudRate(ItemIndex);
      stDataBits: FReceiver.DataBits:= TDataBits(ItemIndex);
      stStopBits: FReceiver.StopBits:= TStopBits(ItemIndex);
      stParity: FReceiver.Parity:= TParity(ItemIndex);
      stFlowControl: FReceiver.FlowControl:= TFlowControl(ItemIndex);
    end;
  end;

  inherited Change;
end;

procedure TSerialLabeledComboBox.SetSelectionType(const Value: TSelectionType);
begin
  if Value=FSelectionType then
    Exit;

  FSelectionType:= Value;
  FilloutItems;
  TransmitterSetting;
  if ItemIndex=-1 then
    ReceiverSetting;
end;

procedure TSerialLabeledComboBox.FilloutItems;
begin
  with Items do
  begin
    Clear;
    case FSelectionType of
      stBaudRate: begin
          Append('300 baud');
          Append('600 baud');
          Append('1200 baud');
          Append('2400 baud');
          Append('4800 baud');
          Append('9600 baud');
          Append('14400 baud');
          Append('19200 baud');
          Append('38400 baud');
          Append('57600 baud');
          Append('115200 baud');
          Append('230400 baud');
          Append('460800 baud');
          Append('921600 baud');
          Width:= 100;
      end;
      stDataBits: begin
          Append('7 data bits');
          Append('8 data bits');
          Width:= 100;
      end;
      stStopBits: begin
          Append('1 stop bit');
          Append('2 stop bits');
          Width:= 100;
      end;
      stParity: begin
          Append('none parity');
          Append('odd parity');
          Append('even parity');
          Append('mark parity');
          Append('space parity');
          Width:= 100;
      end;
      stFlowControl: begin
          Append('none flow control');
          Append('RTS/CTS flow control');
          Append('DTR/DSR flow control');
          Append('XON/XOFF flow control');
          Width:= 150;
      end;
    end;
  end;
end;

procedure TSerialLabeledComboBox.SetTransmitter(const Value: TFtdiUsb);
begin
  if Value<>FTransmitter then
  begin
    FTransmitter:= Value;
    TransmitterSetting;
  end;
end;

procedure TSerialLabeledComboBox.SetReceiver(const Value: TFtdiUsb);
begin
  if Value<>FReceiver then
  begin
    FReceiver:= Value;
    if FTransmitter<>nil then
    begin
      if FReceiver<>nil then
      begin
        case FSelectionType of
          stBaudRate: FReceiver.BaudRate:= FTransmitter.BaudRate;
          stDataBits: FReceiver.DataBits:= FTransmitter.DataBits;
          stStopBits: FReceiver.StopBits:= FTransmitter.StopBits;
          stParity: FReceiver.Parity:= FTransmitter.Parity;
          stFlowControl: FReceiver.FlowControl:= FTransmitter.FlowControl;
        end;
      end;
    end
    else
      ReceiverSetting;
  end;
end;

procedure TSerialLabeledComboBox.TransmitterSetting;
begin
  if FTransmitter<>nil then
  begin
    case FSelectionType of
      stBaudRate: ItemIndex:= Integer(FTransmitter.BaudRate);
      stDataBits: ItemIndex:= Integer(FTransmitter.DataBits);
      stStopBits: ItemIndex:= Integer(FTransmitter.StopBits);
      stParity: ItemIndex:= Integer(FTransmitter.Parity);
      stFlowControl: ItemIndex:= Integer(FTransmitter.FlowControl);
    end;
    if FReceiver<>nil then
    begin
      case FSelectionType of
        stBaudRate: FReceiver.BaudRate:= FTransmitter.BaudRate;
        stDataBits: FReceiver.DataBits:= FTransmitter.DataBits;
        stStopBits: FReceiver.StopBits:= FTransmitter.StopBits;
        stParity: FReceiver.Parity:= FTransmitter.Parity;
        stFlowControl: FReceiver.FlowControl:= FTransmitter.FlowControl;
      end;
    end;
  end
  else
    ItemIndex:= -1;
end;

procedure TSerialLabeledComboBox.ReceiverSetting;
begin
  if Receiver<>nil then
  begin
    case FSelectionType of
      stBaudRate: ItemIndex:= Integer(FReceiver.BaudRate);
      stDataBits: ItemIndex:= Integer(FReceiver.DataBits);
      stStopBits: ItemIndex:= Integer(FReceiver.StopBits);
      stParity: ItemIndex:= Integer(FReceiver.Parity);
      stFlowControl: ItemIndex:= Integer(FReceiver.FlowControl);
    end;
  end
  else
    ItemIndex:= -1;
end;

{ TFtdiLabeledComboBox }

procedure TFtdiLabeledComboBox.Change;
begin
  inherited Change;

  if FFtdiUsb<>nil then
    FFtdiUsb.SetFtdiLocationId(TFtdiUsb.UsbDeviceInfoList[ItemIndex].LocID);
end;

procedure TFtdiLabeledComboBox.DoEnter;
begin
  if FAutoUpdate then
    UpdateUsbDevices;

  inherited DoEnter;
end;

procedure TFtdiLabeledComboBox.MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if (Button=mbLeft) and FAutoUpdate then
    UpdateUsbDevices;

  if Button=mbRight then
    CustomHint.ShowHint(ClientToScreen(Point(Width + 50, Height)));

  inherited;
end;

procedure TFtdiLabeledComboBox.SetFtdiUsb(const Value: TFtdiUsb);
begin
  if Value<>FFtdiUsb then
  begin
    if FFtdiUsb<>nil then
      FFtdiUsb.SetFtdiLabeledComboBox(nil);

    FFtdiUsb:= Value;
    if FFtdiUsb=nil then
      Exit;

    FFtdiUsb.SetFtdiLabeledComboBox(Self);
    if CustomHint=nil then
    begin
      CustomHint:= TCustomHint.Create(Self);
      with CustomHint do
      begin
        Title:= 'Version Info';
        Description:= 'DLL version: ' + FFtdiUsb.GetLibraryVersion;
        Delay:= 0;
        HideAfter:= 4000;
      end;
    end;
  end;
end;

procedure TFtdiLabeledComboBox.UpdateUsbDevices;
begin
  if FFtdiUsb=nil then
    Exit;

  Items.Clear;
  Items.AddStrings(FFtdiUsb.GetUsbDeviceList);
  ItemIndex:= -1;
  FFtdiUsb.SetFtdiLocationId(0);
end;

end.
